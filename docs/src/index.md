```@meta
CurrentModule = FilePaths2
```

# FilePaths2

Documentation for [FilePaths2](https://gitlab.com/ExpandingMan/FilePaths2.jl).

```@index
```

```@autodocs
Modules = [FilePaths2]
```
