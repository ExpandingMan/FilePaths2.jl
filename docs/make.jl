using FilePaths2
using Documenter

DocMeta.setdocmeta!(FilePaths2, :DocTestSetup, :(using FilePaths2); recursive=true)

makedocs(;
    modules=[FilePaths2],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/FilePaths2.jl/blob/{commit}{path}#{line}",
    sitename="FilePaths2.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/FilePaths2.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
