using FilePaths2, AbstractTrees

using FilePaths2: PosixPath, walkpath, Parents, PathSpec, parentpath, childpath

using FilePaths2: UsersPermissionMode, PermissionMode



function mktestdir(root=tempdir())
    dir = joinpath(root, "filepaths2")
    rm(dir, recursive=true, force=true)
    mkdir(dir)
    mkdir(joinpath(dir, "abc"))
    mkdir(joinpath(dir, "de"))
    mkdir(joinpath(dir, "de", "f"))
    mkdir(joinpath(dir, "de", "g"))

    open(io -> write(io, "test0"), joinpath(dir, "test0.txt"); write=true)
    open(io -> write(io, "test1"), joinpath(dir, "abc", "test1.txt"); write=true)
    open(io -> write(io, "test2"), joinpath(dir, "de", "test2.txt"); write=true)
    open(io -> write(io, "test3"), joinpath(dir, "de", "f", "test3.txt"); write=true)

    dir
end
