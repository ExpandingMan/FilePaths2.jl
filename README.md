# FilePaths2

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/FilePaths2.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/FilePaths2.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/FilePaths2.jl/-/pipelines)

An experimental implementation of typed file paths in Julia.

## Notice
This package is currently *deliberately* incomplete while I solicit opinions from relevant parts of
the Julia package ecosystem.  What you see here represents a bare-minimum working example,
ultimately my intention is for this to cover (at least) all of the functionality of
FilePathsBase.jl.

## Overview
There are a number of problems with existing implementations which this package tries to solve.

### All paths are tree nodes
This package requires that all path types must be trees implementing the
[AbstractTrees.jl](https://github.com/JuliaCollections/AbstractTrees.jl) interface (v0.4).  This
ensures that there are strict guarantees on the behavior of path types for functions with default
methods such as `walkpath` which simply use methods from AbstractTrees.jl rather than
re-implementing tree algorithms.

### Strict semantics governing what is inferable from strings
One of the few things that all file path types have in common is that they all can be described by
strings giving a complete path from the root.  Taking advantage of this is quite useful in that it
guarantees that certain information is always available, for example, all ancestors of every
absolute path can always be inferred.

Most `FilePaths2.AbstractPath` objects are expected to use the `PathSpec` struct, which is a
convenience wrapper around the string describing the path and can be used to perform certain
operations from parsing the string alone such as finding the parent directory or determine whether
one path can be an antecedent of another.

**A major caveat following from this is that path strings used to construct path objects must either
be absolute or must exist so that the absolute path can be inferred.**

### Strict semantics about when the program can make remote calls
For local file systems it is usually taken for granted that updates are cheap, but this becomes a
major obstacle for remote file systems.  Therefore, only the following functions are permitted to
make remote calls, and they must accept the `update` keyword even if `update=false` results in an
error (e.g. because the path object doesn't contain the information it needs):
- `AbstractTrees.children`
- `readdir`
- `walkpath`
- `ispath`
- `isfile`
- `isdir`
- *(I'm not yet confident this is a complete list.)*


## FAQ

### Why another package? Please no more PackageName2!
The current status of this project is as an RFC to developers of related packages to see if we can
come to a consensus on what is needed.  I would be perfectly happy to rewrite FilePathsBase based on
this, but at the time of writing I have no idea if anyone would go along with that.

### What's wrong with FilePathsBase.jl?
FilePathsBase.jl works great for local paths (what are called `SystemPath` by FilePathsBase and
FilePaths2) but it does not generalize easily to other path types.  For one, it tends to assume that
updating the objects in memory is very cheap, but for most remote file systems these operations are,
on the contrary, *very* expensive and usually involve network calls.  Another major problem is that
some things which we'd like to treat as "remote file systems" (notably S3) are in fact completely
different types of data structures and implementing a filesystem-like abstraction on top of them
requires a great deal of care.  FilePathsBase is arguably too ambitious in expecting all paths to
have all the properties of local file system paths.

### Why disallow relative paths?
I'm taking the perspective that relative paths are merely a "pun".  They have a lot of undesirable
properties, in particular, they are dependent on a global state which may not even be well defined
(i.e. it makes no sense on remote file systems).  Using relative paths also provides fewer
guarantees about what can be inferred from the path, for example you can not, in general determine
the parent directory from a relative path.

**You can still use relative paths as constructors and methods are provided to show relative paths
(we might even print them as relative by default, I haven't decided on this).**

### How do you deal with key-value stores like S3?
S3 is a key-value store that sometimes masquerades as a file system, a fact that has been the cause
of much human suffering.  The approach I took here was to ask "how do we construct a tree given
*only* the data that the API provides?".  In the case of all key-value stores, the API must be able
to provide the keys, possibly in path form.  The [proof-of-concept algorithm in
FilePaths2](https://gitlab.com/ExpandingMan/FilePaths2.jl/-/blob/main/src/s3path.jl) uses the list
of key strings *ONLY* to infer the full tree structure.  This imposes some constraints on the
structure of the resulting tree, for example, there can be no empty directories (at least none that
are actually directories and not just objects with keys that look like directories), but one of the
benefits of this approach is that questions such as `isdir` are still well-defined: we can simply
ask whether the would-be directory is the ancestor of an existing leaf node.

### Why do you insist on treating S3 like a file system?
I have been forced to do this by existing software.  The brutal truth is that software compares S3
to file system *all the time*, e.g. when you sync a local directory to S3.  I consider the fact that
S3 does not know it is a tree to be a major design blunder, not because I think there is anything
wrong with key-value stores, but because S3 is too often not used that way.  Essentially, I likely
agree with criticisms that S3 is not a file system so don't treat it as such, but I have to work
with software that leaves me no choice.

### Ok, even then, why bother with one package with such disparate use cases?
In light of the above, it is absolutely vital for us to be able to handle local file systems and
remote ones such as S3 or HDFS in a generic way.  Doing the "same thing" with a file in S3 that you
just did with a file on your local file system is *way* too common a use case for it to be
acceptable (in my mind) to have a path type that cannot accommodate both cases.
