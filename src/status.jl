
abstract type AbstractStatus end

Base.filemode(s::AbstractStatus) = s.mode
Base.filesize(s::AbstractStatus) = s.size  # in bytes
Base.mtime(s::AbstractStatus) = s.mtime
Base.ctime(s::AbstractStatus) = s.ctime


struct UsersPermissionMode
    read::Bool
    write::Bool
    execute::Bool
end

UsersPermissionMode(x::Unsigned) = UsersPermissionMode(0b100 & x > 0, 0b010 & x > 0, 0b001 & x > 0)

function _permission_mode_char(c::Char, name::Char)
    c == name && return true
    c == '-' && return false
    throw(ArgumentError("invalid permission mode character: '$c'"))
end

function UsersPermissionMode(s::AbstractString)
    length(s) == 3 || throw(ArgumentError("user permission mode string can only consist of 3 characters; found \"$s\""))
    UsersPermissionMode(_permission_mode_char(s[1], 'r'), _permission_mode_char(s[2], 'w'),
                        _permission_mode_char(s[3], 'x'))
end

struct PermissionMode
    owner::UsersPermissionMode
    group::UsersPermissionMode
    other::UsersPermissionMode
end

function PermissionMode(x::Unsigned)
    PermissionMode(UsersPermissionMode(x >> 6), UsersPermissionMode(x >> 3), UsersPermissionMode(x))
end

function PermissionMode(s::AbstractString)
    n = lastindex(s)
    PermissionMode(UsersPermissionMode(view(s, (n-8):(n-6))),
                   UsersPermissionMode(view(s, (n-5):(n-3))),
                   UsersPermissionMode(view(s, (n-2):n)),
                  )
end
