
struct Parents{T<:AbstractPath}
    path::T
end

Base.IteratorEltype(::Type{<:Parents}) = Base.HasEltype()
Base.eltype(::Type{Parents{T}}) where {T} = T

Base.IteratorSize(::Type{<:Parents}) = Base.SizeUnknown()

function Base.iterate(ps::Parents)
    p = parent(ps.path)
    isnothing(p) ? nothing : (p, p)
end
function Base.iterate(ps::Parents, s)
    p = parent(s)
    isnothing(p) ? nothing : (p, p)
end

