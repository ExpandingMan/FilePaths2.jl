
abstract type SystemPath <: AbstractPath end

# can update
Base.ispath(p::SystemPath; kw...) = ispath(string(p))
Base.isfile(p::SystemPath; kw...) = isfile(string(p))
Base.isdir(p::SystemPath; kw...) = isdir(string(p))
Base.pwd(::Type{P}) where {P<:SystemPath} = P(pwd())

Base.islink(p::SystemPath; kw...) = islink(string(p))
Base.ismount(p::SystemPath; kw...) = ismount(string(p))
Base.issocket(p::SystemPath; kw...) = issocket(string(p))
Base.isfifo(p::SystemPath; kw...) = isfifo(string(p))

Base.mkdir(p::SystemPath; kw...) = mkdir(string(p))
Base.touch(p::SystemPath; kw...) = touch(string(p))

Base.rm(p::SystemPath) = rm(string(p))

Base.tempname(::Type{P}, p=tempdir(); kw...) where {P<:SystemPath} = P(tempname(p; kw...))
Base.tempdir(::Type{P}) where {P<:SystemPath} = P(tempdir())
function Base.mktemp(::Type{P}, p=tempdir(); kw...) where {P<:SystemPath}
    (path, io) = mktemp(p; kw...)
    (P(path), io)
end
Base.mktemp(𝒻, ::Type{P}, p=tempdir()) where {P<:SystemPath} = mktemp((path, io) -> 𝒻(P(path), io), p)

Base.read(p::SystemPath; kw...) = read(string(p); kw...)

Base.write(p::SystemPath, a...) = write(string(p), a...)

Base.cp(p::SystemPath, q::SystemPath; kw...) = cp(string(p), string(q); kw...)


struct PosixPath <: SystemPath
    path::PathSpec
end

PosixPath(str::AbstractString) = PosixPath(str |> abspath |> PathSpec)

PathSpec(p::PosixPath) = p.path

Base.print(io::IO, p::PosixPath) = print(io, p.path)

AbstractTrees.parent(q::PosixPath) = PosixPath(parentpath(q.path))

function AbstractTrees.children(p::PosixPath; update::Bool=true)
    update || throw(ArgumentError("PosixPath must update to get children"))
    if isfile(p)
        ()
    elseif isdir(p)
        Iterators.map(c -> joinpath(p, c), readdir(abspath(p)))
    else
        throw(ArgumentError("path $p is not a file or a directory"))
    end
end


struct PosixStatus <: AbstractStatus
    size::Int  # in bytes
    device::UInt64
    inode::UInt64
    mode::PermissionMode
    nlink::Int
    uid::UInt64
    gid::UInt64
    rdev::UInt64
    blksize::Int
    blocks::Int
    mtime::DateTime
    ctime::DateTime
end

function PosixStatus(s::Base.Filesystem.StatStruct)
    PosixStatus(Int(s.size),
                s.device,
                s.inode,
                PermissionMode(s.mode),
                Int(s.nlink),
                s.uid,
                s.gid,
                s.rdev,
                Int(s.blksize),
                Int(s.blocks),
                unix2datetime(s.mtime),
                unix2datetime(s.ctime),
               )
end

Base.stat(p::PosixPath) = PosixStatus(stat(string(p)))
