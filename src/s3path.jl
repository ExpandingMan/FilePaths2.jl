
#====================================================================================================
TODO

this wouldn't be here in real life, it's just a forum for me to figure out how it should look...

Also for simplicity here I will omit the leading `s3:/` and test this implementation on the
local file system
====================================================================================================#

# this is like what S3 can give us
# it's not efficient and I don't have to care...
function _simulate_s3_leaf_nodes(dir::AbstractString)
    isdir(dir) || return String[]
    dir = abspath(dir)
    o = String[]
    for (root, dirs, files) ∈ walkdir(dir)
        for f ∈ files
            push!(o, joinpath(root, f))
        end
    end
    PathSpec.(sort!(o))
end


struct S3Path <: AbstractPath
    root::PathSpec
    children::Vector{S3Path}
end

function _s3path(str::PathSpec, paths::AbstractVector{PathSpec}, idx::Integer=1)
    ch = S3Path[]
    j = idx
    while j ≤ length(paths)
        q = paths[j]
        str.knowndir || return (S3Path(str, []), j+1)
        np = childpath(str, q)
        isnothing(np) && break
        (p, j′) = _s3path(np, paths, j)
        push!(ch, p)
        j = j′
    end
    (S3Path(str, ch), j)
end

function S3Path(str::AbstractString)
    # this is a hack that obviously has to be done more carefully when really doing S3
    str = abspath(str)
    str *= isdir(str) ? "/" : ""
    _s3path(PathSpec(str), _simulate_s3_leaf_nodes(str))[1]
end

PathSpec(p::S3Path) = p.root

function Base.ispath(p::S3Path; update::Bool=true)
    udpate ? ispath(abspath(p)) : true
end

AbstractTrees.children(p::S3Path; update::Bool=true) = p.children

