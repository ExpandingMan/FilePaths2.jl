
abstract type AbstractPath end

separator(::Type{<:AbstractPath}) = "/"
separator(p::AbstractPath) = separator(typeof(p))
rootstring(p::AbstractPath) = separator(p)

# the following functions are not allowed to update
Base.abspath(p::AbstractPath) = string(PathSpec(p))
function Base.basename(p::AbstractPath)
    ps = PathSpec(p)
    isroot(p) ? ps.rootstring : last(ps.segments)
end
Base.normpath(p::AbstractPath) = string(PathSpec(p))
Base.joinpath(p::AbstractPath, a...) = typeof(p)(joinpath(PathSpec(p), a))

AbstractTrees.NodeType(::Type{<:AbstractPath}) = HasNodeType()
AbstractTrees.nodetype(::Type{P}) where {P<:AbstractPath} = P

AbstractTrees.ParentLinks(::Type{<:AbstractPath}) = StoredParents()

AbstractTrees.parent(p::AbstractPath) = typeof(p)(parentpath(PathSpec(p)))

# the following functions *can* update
Base.readdir(p::AbstractPath; join::Bool=false) = map(join ? string : basename, children(p))
walkpath(p::AbstractPath) = PreOrderDFS(p)

Base.show(io::IO, p::AbstractPath) = print(io, typeof(p), "(\"", PathSpec(p), "\")")

isknowndir(p::AbstractPath) = isknowndir(PathSpec(p))

Base.filemode(p::AbstractPath; kw...) = filemode(stat(p; kw...))
Base.filesize(p::AbstractPath; kw...) = filesize(stat(p; kw...))
Base.mtime(s::AbstractPath; kw...) = mtime(stat(p; kw...))
Base.ctime(s::AbstractPath; kw...) = ctime(stat(p; kw...))

#TODO: ideally a fall-back sync method should be here
