
struct PathSpec
    rootstring::String
    separator::String
    segments::Vector{SubString{String}}
    knowndir::Bool  # this being true is a sufficient but not a necessary condition to be a dir
end

function PathSpec(str::AbstractString, sep::AbstractString="/", rs::AbstractString=sep; knowndir::Bool=endswith(str, sep))
    j = startswith(str, rs) ? length(rs)+1 : 1  # we don't require starting with the root string
    ss = filter(!isempty, split(str[j:end], sep))
    PathSpec(rs, sep, ss, knowndir)
end

function Base.print(io::IO, ps::PathSpec)
    print(io, ps.rootstring, join(ps.segments, ps.separator))
    !isempty(ps.segments) && ps.knowndir && print(io, ps.separator)
    nothing
end

function Base.show(io::IO, ps::PathSpec)
    show(io, typeof(ps))
    print(io, "(\"", ps, "\")")
end

function Base.joinpath(ps::PathSpec, as::Union{AbstractVector{<:AbstractString},Tuple};
                       knowndir::Bool=endswith(last(as), ps.separator),
                      )
    ss = copy(ps.segments)
    for a ∈ as
        a′ = filter(!isempty, split(a, ps.separator))
        append!(ss, a′)
    end
    PathSpec(ps.rootstring, ps.separator, ss, knowndir)
end
Base.joinpath(ps::PathSpec, a...; isdir::Bool=endswith(last(a), ps.separator)) = joinpath(ps, a; isdir)

"""
    isknowndir(p)

Whether it is known that the path `p` is a directory if it exists.  If `isknowndir(p)` is `false`,
`p` may still be a directory.  It is assumed that any path for which `isknowndir` is allowed to
append a trailing separator to its path string (for example: `/tmp` is equivalent to `/tmp/`).

This function does *not* check whether `p` exists, but applies to properties `p` *would* have if
it did exist.
"""
isknowndir(ps::PathSpec) = ps.knowndir


"""
    parentpath(ps::PathSpec)

Return the `PathSpec` of the path which is the parent of the one specified by `ps`.  Returns `nothing` if `ps` is
the root.
"""
function parentpath(ps::PathSpec)
    isempty(ps.segments) && return nothing
    ss = filter(!isempty, ps.segments[1:(end-1)])
    PathSpec(ps.rootstring, ps.separator, ss, true)
end

"""
    childpath(ps::PathSpec, qs::PathSpec)

Find the `PathSpec` of the child of the path specified by `ps` which is also an ancestor of the path
specified by `qs`.  If no such path can exist, returns `nothing`.

Note that this does *not* check whether `ps` and `qs` have common root or separator strings, so this can be used
even for `PathSpec`s for which these strings are mismatched.

## Examples
```julia
childpath(PathSpec("/a/b/"), PathSpec("/a/b/c"))  # returns PathSpec("/a/b/c")
childpath(PathSpec("/a/b/"), PathSpec("/a/b/c/d"))  # returns PathSpec("/a/b/c/")
childpath(PathSpec("/a/b"), PathSpec("/a/b/c"))  # returns nothing because PathSpec("/a/b") is a leaf
childpath(PathSpec("/a/b"), PathSpec("/α/β"))  # returns nothing because first arg is not an encestor of second
```
"""
function childpath(ps::PathSpec, qs::PathSpec)
    ps.knowndir || return nothing
    m = length(ps.segments)
    n = length(qs.segments)
    n ≥ m || return nothing
    qs.segments[1:m] == ps.segments[1:m] || return nothing
    knowndir = n == m+1 ? qs.knowndir : true
    PathSpec(ps.rootstring, ps.separator, qs.segments[1:min(m+1, n)], knowndir)
end

