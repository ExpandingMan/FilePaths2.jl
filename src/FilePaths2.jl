module FilePaths2


using Dates
using AbstractTrees
using AbstractTrees: parent

using Infiltrator


include("pathspec.jl")
include("status.jl")
include("abstractpath.jl")
include("iterators.jl")
include("systempath.jl")


# this obviously wouldn't be included in a real package, I just want
# to see how it looks
include("s3path.jl")


export AbstractPath, SystemPath, PosixPath
export walkpath

#====================================================================================================
       TODO:

The following are a bit more complicated and still need to be implemented:
- `relpath`
- `sync`
====================================================================================================#

end
